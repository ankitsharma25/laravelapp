@extends('layouts.admin')

    @section('content')
        <h2>Media </h2>
        @if($photos)
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($photos as $photo)
                        <tr>
                            <td>{{$photo->id}}</td>
                            <td><img style="max-width:200px" height="50" class="img-responsive img-rounded" src="{{$photo->file ? $photo->file : '/images/default.png'}}" /></td>
                            <td>{{$photo->created_at}}</td>
                        </tr>
                        <tr>
                            <td>
                                {!! Form::open(['method'=>'DELETE','action'=>['AdminMediaController@destroy',$photo->id]]) !!}
                                    {!! Form::submit('Delete',['class'=>'btn btn-primary']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        @endif
    @endsection