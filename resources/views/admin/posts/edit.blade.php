@extends('layouts.admin')

@section('content')
    <h1>Edit Posts</h1>

    <div class="col-sm-3">
        <img src="{{$post->photo ? $post->photo->file : '/images/default.png'}}" alt="" class="img-responsive img-rounded" />
    </div>
    <div class="col-sm-9">
    {!! Form::model($post,['method'=>'PATCH','action'=>['AdminPostsController@update',$post->id],'files'=>'true']) !!}
    <div class="form-group">
        {!! Form::label('title','Title :') !!}
        {!! Form::text('title',null,['class'=>'form-control','rows'=>3]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('body','Body :') !!}
        {!! Form::textarea('body',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('category_id','Category ') !!}
        {!! Form::select('category_id',[0=>'select']+$categories,null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('photo_id','File:') !!}
        {!! Form::file('photo_id',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Create Post',['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
    <div class="row text-center">
        {!! Form::open(['method'=>'DELETE','action'=>['AdminPostsController@destroy',$post->id]]) !!}
        {!! Form::submit('Delete Post',['class'=>'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
    </div>
    @include('includes.form_error')


@endsection