@extends('layouts.admin')

    @section('content')
    <h1>Posts</h1>
    @if(Session::has('deleted_post'))
        <p class="bg bg-danger">{{session('deleted_user')}}</p>
    @endif
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Photo</th>
                <th>Title</th>
                <th>Body</th>
                <th>Category</th>
                <th>Photo</th>
                <th>Author</th>
                <th>Created at</th>
                <td>Updated at</td>
            </tr>
            </thead>
            <tbody>
            @if($posts)
                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td><img height="50" src="{{$post->photo ? $post->photo->file : "/images/default.png"}}" alt=""></td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->body}}</td>
                        <td>{{$post->category ? $post->category->name : 'No category'}}</td>
                        <td><a href="{{route('home.post',$post->slug)}}">View Post</a></td>
                        <td><a href="{{route('admin.comments.show',$post->id)}}">View Comments</a></td>
                        <td>{{$post->created_at->diffForHumans()}}</td>
                        <td>{{$post->updated_at->diffForHumans()}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-5">
            {{$posts->render()}}
        </div>
    </div>
    @endsection