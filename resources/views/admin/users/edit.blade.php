@extends('layouts.admin')

@section('content')
    <h1>Edit Users</h1>
    <div class="col-sm-3">
        <img src="{{$user->photo ? $user->photo->file : '/images/default.png'}}" alt="" class="img-responsive img-rounded" />
    </div>
    <div class="col-sm-9">
    <!--Model Binding-->
    {!! Form::model($user,['method'=>'PATCH','action'=>['AdminUsersController@update',$user->id],'files'=>'true']) !!}
    <div class="form-group">
        {!! Form::label('name','Name') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Email') !!}
        {!! Form::email('email',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('is_active','Status') !!}
        {!! Form::select('is_active',[1=>'Active',0=>'Not Active'],0,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('role_id','Role: ') !!}
        {!! Form::select('role_id',[''=>'Choose Options']+$roles,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password','Password:') !!}
        {!! Form::password('password',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('photo_id','File:') !!}
        {!! Form::file('photo_id',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Create Post',['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
    </div>
     <div class="row text-center">
         {!! Form::open(['method'=>'DELETE','action'=>['AdminUsersController@destroy',$user->id]]) !!}
            {!! Form::submit('Delete user',['class'=>'btn btn-danger']) !!}
         {!! Form::close() !!}
     </div>

    <div class="row">
        @include('includes.form_error')
    </div>


@endsection