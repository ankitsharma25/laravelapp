@extends('layouts.blog-post')

@section('content')
    <h1>Post</h1>
    <div class="col-lg-8">

        <!-- Blog Post -->

        <!-- Title -->
        <h1>{{$post->title}}</h1>

        <!-- Author -->
        <p class="lead">
            by <a href="#">{{$post->user->name}}</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span> Posted {{$post->created_at->diffForHumans()}}</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-responsive" src="{{$post->photo->file}}" alt="">

        <hr>

        <!-- Post Content -->
        <p>{{$post->body}}</p>

        <hr>
        @if(Session::has('comment_message'));
            {{session('comment_message')}}
        @endif
        <!-- Blog Comments -->
        @if(Auth::check())
        <!-- Comments Form -->
        <div class="well">
            <h4>Leave a Comment:</h4>
            {!! Form::open(['method'=>'Post','action'=>'PostCommentsController@store']) !!}
            <input type="hidden" name="post_id" name="" value="{{$post->id}}" >
                <div class="form-group">
                    {!! Form::label('body','Body :') !!}
                    {!! Form::textarea('body',null,['class'=>'form-control','rows'=>3]) !!}
                </div>
                    {!! Form::submit('Create Post',['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
        @endif
        <hr>

        <!-- Posted Comments -->
        @if(count($comments) > 0)
        <!-- Comment -->
        @foreach($comments as $comment)
        <div class="media">
            <a class="pull-left" href="#">
                <img height="50" class="media-object" src="{{$comment->photo}}" alt="{{$comment->photo}}">
            </a>
            <div class="media-body">
                <h4 class="media-heading">{{$comment->author}}
                    <small>{{$comment->created_at->diffForHumans()}}</small>
                </h4>
                {{$comment->body}}






            <!-- Nested Comment -->
                <div class="media">
                    @if(count($comment->replies) > 0)
                        @foreach($comment->replies as $reply)
                    <a class="pull-left" href="#">
                        <img height="30" class="media-object" src="{{$reply->photo}}" alt="{{$reply->photo}}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$reply->author}}
                            <small>{{$reply->created_at->diffForHumans()}}</small>
                        </h4>
                       {{$reply->body}}
                    </div>
                    @endforeach
                    @endif
                    <div class="comment-reply-container">
                        <button class="toggle-reply btn btn-primary pull-right">
                            Reply
                        </button>
                        <div class="comment-reply">
                            {!! Form::open(['method'=>'Post','action'=>'CommentRepliesController@createReply']) !!}
                            <input type="hidden" name="comment_id" name="" value="{{$comment->id}}" >
                            <div class="form-group">
                                {!! Form::label('body','Body :') !!}
                                {!! Form::textarea('body',null,['class'=>'form-control','rows'=>1]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Reply',['class'=>'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
                <!-- End Nested Comment -->








            </div>
        </div>
            @endforeach
        @endif



    </div>
    @endsection

@section('scripts')
    <script>
        $('.comment-reply-container .toggle-reply').click(function () {
            $(this).next().slideToggle("slow");
        });
    </script>
    @stop